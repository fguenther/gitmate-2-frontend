import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { ApiService } from './../api/api.service';

@Component({
  selector: 'array-setting',
  templateUrl: './array-setting.component.html',
  styleUrls: ['./array-setting.component.css']
})
export class ArraySettingComponent implements OnInit {
  @Input() description: string;
  @Input() backend_list: any;
  @Input() repoid: number;
  @Input() plugin_name: string;
  @Input() setting_name: string;
  @Input() item_desc: string;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.ensure_last();
  }

  delete_setting(setting) {
    const index = this.backend_list.indexOf(setting, 0);
    if (index > -1) {
      this.backend_list.splice(index, 1);
    }
    this.update(null);
  }

  update(setting) {
    if (this.is_empty(setting)) {
      this.delete_setting(setting);
    }
    this.ensure_last();
    this.make_request();
  }

  ensure_last(focus = false) {
    if (!this.is_empty(this.backend_list[this.backend_list.length - 1])) {
      this.backend_list.push('');
    } else if (this.is_empty(this.backend_list[this.backend_list.length - 2])) {
      this.backend_list.pop();
    }
  }

  is_empty(setting) {
    return setting === '';
  }

  make_request() {
    const bl = [];
    for (const item of this.backend_list) {
      if (item !== '') {
        bl.push(item);
      }
    }
    this.apiService.setPluginSetting(this.plugin_name, this.repoid,
                                    this.setting_name, bl)
                                    .subscribe(plugins => console.log(plugins));
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }
}
